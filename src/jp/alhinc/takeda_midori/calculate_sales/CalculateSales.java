package jp.alhinc.takeda_midori.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class CalculateSales {
	public static void main(String[] args) {

		TreeMap<String, String> nameMap = new TreeMap<String, String>();
		TreeMap<String, Long> salesMap = new TreeMap<String, Long>();

		//コマンド引数が存在しない、またはコマンド引数が2つ以上ある場合のエラー処理
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");

			return;
		}

		//ファイルの入力
		BufferedReader br = null;
		try {
			File shitenFile = new File(args[0],"branch.lst");

			//支店定義ファイルが存在しないときのエラー処理
			if(!shitenFile.exists()) {

				System.out.println("支店定義ファイルが存在しません");

				return;
			}

			FileReader fr = new FileReader(shitenFile);
			br = new BufferedReader(fr);

			//支店コードと支店名を分割
			String shitenline;
			while ((shitenline = br.readLine()) != null) {
				String [] keys = shitenline.split(",");

					//支店定義ファイルのフォーマットが異なるときのエラー処理
					if(keys.length != 2 || !keys[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");

					return;
					}

				//分割したデータをmapに格納
				nameMap.put(keys[0], keys[1]);
				salesMap.put(keys[0], (long) 0);

				}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");

		} finally {
			if(br != null) {
				try {

					br.close();

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					}
				}
			}

		//売上ファイルの読込
		File uriageFile = new File(args[0]);
		File[] uriageList = uriageFile.listFiles();
		List<File> uriageLists = new ArrayList<File>();

		//ファイル名が8桁の数字、.rcdのファイルを抽出
		for(int i=0; i<uriageList.length; i++) {
			if(uriageList[i].getName().matches("^[0-9]{8}.rcd$") && uriageList[i].isFile()) {
				uriageLists.add(uriageList[i]);
			}
		}

			//ファイル名が連番でないときのエラー処理
			for(int i=0; i<uriageLists.size() -1; i++) {

				String name1 = uriageLists.get(i).getName();
				String name2 = uriageLists.get(i + 1).getName();

				String number1 = name1.substring(0, 8);
				String number2 = name2.substring(0, 8);

				int num1 = Integer.parseInt(number1);
				int num2 = Integer.parseInt(number2);

				if(num2-num1 != 1) {
					System.out.println("売上ファイル名が連番になっていません");

					return;
				}
			}

		//uriage抽出ファイルの読込
		for(File uriageStr : uriageLists) {
			try {
				FileReader fr = new FileReader(uriageStr);
				br = new BufferedReader(fr);
				String uriageLine;

				ArrayList<String> uriageKey = new ArrayList<String>();

					while ((uriageLine = br.readLine()) != null) {
						uriageKey.add(uriageLine);

					}
						//売上ファイルの中身が3行以上ある場合のエラー処理
						if(uriageKey.size() != 2) {

							System.out.println(uriageStr.getName() + "のフォーマットが不正です");

							return;
						}


						//売上金額に数値以外が含まれている場合のエラー
						if(!uriageKey.get(1).matches("^[0-9]+$")){

							System.out.println("予期せぬエラーが発生しました");

							return;
						}

						//支店コードが該当しないときのエラー処理
						if(!salesMap.containsKey((uriageKey.get(0)))) {

							System.out.println(uriageStr.getName() +  "の支店コードが不正です");

							return;
						}

				//Keyが一致する売上を合算→売上valueをsalesMapに格納
			    Long value = salesMap.get(uriageKey.get(0)) + Long.parseLong(uriageKey.get(1));
			    salesMap.put(uriageKey.get(0), value);


			    	//合算金額が10桁を超えたときのエラー処理
			    	if(value > 9999999999L) {
			    		System.out.println("合計金額が10桁を超えました");

			    		return;
			    	}

			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");

			}finally {
				if(br != null) {
					try {

						br.close();

					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						}
					}
			}
		}


		//売上集計ファイルへの出力
		BufferedWriter bw = null;

		try {
			File shukeiFile = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(shukeiFile);
			bw = new BufferedWriter(fw);

			for(String key : nameMap.keySet()) {
				/*System.out.println(key +"," + nameMap.get(key) +"," + salesMap.get(key));*/
				bw.write(key +"," + nameMap.get(key) +"," + salesMap.get(key));

				bw.newLine();
			}

        //エラー終了で集計ファイルを作成しない処理 & ファイルロック時のエラー処理
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");

		}finally {

			if(bw != null) {
				try {
					bw.close();

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");

					}
				}
		}
		if (!outPut(args[0], "bransh.out", nameMap, salesMap)) {
			return;
		}
	}

	//集計ファイル出力のメソッド
	public static boolean outPut(String filePath, String fileName,
			                        TreeMap<String, String> nameMap,
			                        TreeMap<String, Long> salesMap){

		BufferedWriter bw = null;

		try {
			File shukeiFile = new File(filePath, fileName);
			FileWriter fw = new FileWriter(shukeiFile);
			bw = new BufferedWriter(fw);

			for(String key : nameMap.keySet()) {

				bw.write(key +"," + nameMap.get(key) +"," + salesMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {

			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}